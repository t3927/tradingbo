package kgfsl.bos.constant;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class CommonConstants {
	
	public static final DateFormat DD_MM_YYYY = new SimpleDateFormat("dd-MM-yyyy");
	
	public static final DateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
	
	public static final SimpleDateFormat HH_MM_SS = new SimpleDateFormat("hh:mm:ss");
	
	public static final DecimalFormat FOUR_DECIMAL = new DecimalFormat("#.####");
	
	public static final DecimalFormat TWO_DECIMAL = new DecimalFormat("#.##");
	
	
}
