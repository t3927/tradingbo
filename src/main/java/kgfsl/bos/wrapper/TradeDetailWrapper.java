package kgfsl.bos.wrapper;

import lombok.Data;

@Data
public class TradeDetailWrapper {

	Double stampDutyVal;
	String symbol;
	String series;
	String clientCode;
	
}
