package kgfsl.bos.wrapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class FileLogWrapper {

	private String information = "";
	private String mismatch = "";
	private String error = "";
	private String fileName = "";
	private Date fileDate;
	private Date uploadedAt;
	private String uploadedBy;
	private String status;
	private Long invalidRecords;
	private Long validRecords;
	private Long totalRecords;
	private String fileCode;
	private String fileDescription;

	@JsonIgnore
	public List<String> writeLog(List<String> writer, String logPath) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");

		writer.add("	" + "File Name		:" + getFileName());
		writer.add("	" + "File Status	    :" + getStatus());		
		writer.add("	" + "File Date		:"
				+ (getFileDate() == null ? "" : sdf.format(getFileDate())));
		writer.add("	" + "Uploaded By	    :" + getUploadedBy());
		writer.add("	" + "Uploaded At	    :"
				+ (getUploadedAt() == null ? "" : timeFormat.format(getUploadedAt())));
		writer.add("	" + "Log Path		:" + logPath);
		return writer;
	}

}

