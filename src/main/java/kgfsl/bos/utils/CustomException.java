package kgfsl.bos.utils;

public class CustomException extends Exception {

	private static final long serialVersionUID = 5723461804119799132L;

	public CustomException() {
		super();
	}

	public CustomException(String message) {
		super(message);
	}

	public CustomException(Throwable cause) {
		super(cause);
	}

	public CustomException(String message, Throwable cause) {
		super(message, cause);
	}

}
