package kgfsl.bos.utils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgreSQL95Dialect;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Component;

import kgfsl.bos.constant.CommonConstants;
import kgfsl.bos.entity.mysql.StorageExchangeSegmentMaster;
import kgfsl.bos.entity.mysql.StorageFileConfig;
import kgfsl.bos.entity.mysql.StorageFileUploadAudit;
import kgfsl.bos.repository.mysql.ExchangeSegmentRepository;
import kgfsl.bos.repository.mysql.FileConfigRepository;
import kgfsl.bos.repository.mysql.FileUploadAuditRepository;
import kgfsl.bos.wrapper.FileLogWrapper;

@Component
public class CommonMethods {	
		
	@Autowired
	FileUploadAuditRepository auditRepo;
	
	@Autowired
	FileConfigRepository fileRepo;	
	
	@Autowired
	ExchangeSegmentRepository exchSegRepo;

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");	
	

	
	public static String getSessionId() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date now = new Date();
		String sessionId = sdf.format(now);
		return sessionId;
	}
	
	public static String getDialect(String database) {
		String dbStringLowerCase = "";
		if (database != "" || database != null) {
			dbStringLowerCase = database.toLowerCase();
			switch (dbStringLowerCase) {
			case "oracle":
				return Oracle10gDialect.class.getName();
			case "mysql":
				return MySQL5Dialect.class.getName();
			case "sql":
				return SQLServer2008Dialect.class.getName();
			case "postgresql":
				return PostgreSQL95Dialect.class.getName();
			default:
				throw new IllegalArgumentException("Dialect not found for " + database + "!");
			}
		} else
			throw new IllegalArgumentException("Dialect not found!!!");
	}

	public static Database getJPAVendor(String database) {
		String dbStringLowerCase = "";
		if (database != "" || database != null) {
			dbStringLowerCase = database.toLowerCase();
			switch (dbStringLowerCase) {
			case "oracle":
				return Database.ORACLE;
			case "mysql":
				return Database.MYSQL;			
			case "postgresql":
				return Database.POSTGRESQL;
			default:
				throw new IllegalArgumentException("Dialect not found for " + database + "!");
			}
		} else
			throw new IllegalArgumentException("Database vendor not found!!!");
	}

	
	public static java.sql.Date dateConvertion(Date date) {
		if (date != null && !date.equals("")) {
			java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
			return sqlStartDate;
		} else
			return null;
	}	
	
 	 public StorageFileUploadAudit writeAndGetAudit(FileLogWrapper fileLog)throws IOException, Exception {
		StorageFileConfig fileConfig =  this.fileRepo.findByFileCode(fileLog.getFileCode());
		StorageFileUploadAudit audit = new StorageFileUploadAudit();
		StorageFileUploadAudit lastRec = auditRepo.findTop1ByFileCodeAndFileDateOrderByUploadAtDesc(fileConfig.getFileCode(), fileLog.getFileDate());
		if(lastRec != null)
		{
			lastRec.setEndTime(new Date());
			this.auditRepo.save(lastRec);
			
			long getId = lastRec.getId();			
			audit.setFileCode(fileConfig.getFileCode());
			audit.setFileDate(fileLog.getFileDate());
			if (fileConfig.getFileName() != null)
				audit.setFileName(fileConfig.getFileName());
			audit.setProcessDate(new Date());
			audit.setUploadBy(fileLog.getUploadedBy());
			audit.setUploadAt(new Date());
	
			audit.setTotalRecords(fileLog.getTotalRecords());
			audit.setInValidRecords(fileLog.getInvalidRecords());
			audit.setValidRecords(fileLog.getValidRecords());
			audit.setUploadStatus(fileLog.getStatus());	
			
			audit.setLogFile(writeLog(fileLog).getBytes());
	
			audit.setId(getId);
			
			this.auditRepo.save(audit);
		}
		return audit;
	}
 	
 	public String writeLog(FileLogWrapper fileLog) throws Exception {
		String logPath = "";
		List<String> writer;		
		Date logTime = new Date();
		SimpleDateFormat pathFormat = new SimpleDateFormat("ddMMMyyyy_HHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
			try {
				writer = new ArrayList<>();
				writer.add("File Code			: " + fileLog.getFileCode());
				writer.add("File Description	: " + fileLog.getFileName());
				writer.add("System Date	        : " + sdf.format(logTime));
				writer.add("File Process Date	: " + dateFormat.format(fileLog.getFileDate()));

				logPath = dateFormat.format(logTime) + "/" + fileLog.getFileCode() + "_" + pathFormat.format(logTime) + ".txt";

				if (fileLog.getStatus() != null) {
					writer.add("");
					writer.add("File Status:");
					writer.add(
							"**************************************************************************************************************************");
					writer = fileLog.writeLog(writer, logPath);
				}

				if (fileLog.getError() != null && !fileLog.getError().isEmpty()) {
					writer.add("");
					writer.add("Error Details:");
					writer.add(
							"**************************************************************************************************************************");
					
					writer.add("	" + fileLog.getError());

				}

				if (fileLog.getMismatch() != null && !fileLog.getMismatch().isEmpty()) {
					writer.add("");
					writer.add("Mismatch Details:");
					writer.add(
							"**************************************************************************************************************************");
					writer = write(writer, fileLog.getMismatch());
				}

				if (fileLog.getInformation() != null && !fileLog.getInformation().isEmpty()) {
					writer.add("");
					writer.add("Information Details:");
					writer.add(
							"**************************************************************************************************************************");
					writer = this.write(writer, fileLog.getInformation());
				}

			} finally {
				
			}
		
		return logPath;
	}
 	

	private List<String> write(List<String> writer, String fileLog) {
			
			writer.add(
					"--------------------------------------------------------------------------------------------------------------------------");
			
			writer.add("		" + fileLog);
			
		
		return writer;
	}
	
	public void fileUploadAuditSave(String status, String fileCode, Date fileDate) {

		StorageFileUploadAudit fileUploadAudit = new StorageFileUploadAudit();
		fileUploadAudit.setUploadStatus(status);
		fileUploadAudit.setFileCode(fileCode);
		fileUploadAudit.setFileDate(fileDate);
		fileUploadAudit.setUploadBy("");
		fileUploadAudit.setUploadAt(new Date());
		this.auditRepo.save(fileUploadAudit);
	}
	
	
	public static Timestamp convert(String str) {
	    try {
	    	DateFormat dateTimeFormat =new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	        Date date = dateTimeFormat.parse(str);
	        return new Timestamp(date.getTime());
	    } catch (ParseException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public static Date convertyyyyMMdd(String str) {
	    try {

			Date date = CommonConstants.DD_MM_YYYY.parse(str);
			String dateString = CommonConstants.YYYY_MM_DD.format(date);			
			return CommonConstants.YYYY_MM_DD.parse(dateString);
	    } catch (ParseException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public StorageExchangeSegmentMaster findByInterOperabilityFlag(){
		StorageExchangeSegmentMaster exchSeg = exchSegRepo.findByInterOperabilityFlag("Y");
		return exchSeg;
	}
	
	public StorageExchangeSegmentMaster findByExchangeCode(String exch){
		StorageExchangeSegmentMaster exchSeg = exchSegRepo.findByExchangeCode(exch);
		return exchSeg;
	}
	
	public StorageExchangeSegmentMaster findByBusinessLineNo(Long busLineNo){
		StorageExchangeSegmentMaster exchSeg = exchSegRepo.findByBusinessLineNo(busLineNo);
		return exchSeg;
	}

}
