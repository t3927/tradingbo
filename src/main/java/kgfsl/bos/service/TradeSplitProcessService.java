package kgfsl.bos.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kgfsl.bos.constant.CommonConstants;
import kgfsl.bos.entity.mysql.StorageExchangeSegmentMaster;
import kgfsl.bos.entity.mysql.StorageSettlementMaster;
import kgfsl.bos.entity.mysql.StorageTradeDetail;
import kgfsl.bos.repository.mysql.SettlementMasterRepository;
import kgfsl.bos.repository.mysql.TradeDetailRepository;
import kgfsl.bos.utils.CommonMethods;
import kgfsl.bos.utils.CustomException;
import kgfsl.bos.wrapper.FileLogWrapper;

@Service
public class TradeSplitProcessService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TradeSplitProcessService.class);	
	
	@Autowired
	SettlementMasterRepository setlRepo;
	
	@Autowired
	CommonMethods commonUtil;
	
	@Autowired
	TradeDetailRepository tradeDetRepo;
	
	@PersistenceContext(unitName = "mysqlEntityManager")
	private EntityManager entityManager;		
	
	public void tradeSplitProcecss(Map<String, String> fileMap) throws IOException, Exception
	{	
		FileLogWrapper fileLog = new FileLogWrapper();		
		Date file_date = null;
		try{		
			file_date = CommonMethods.convertyyyyMMdd(fileMap.get("date"));	
			Long bussLineNo = Long.parseLong(fileMap.get("businessLineNo"));
			
			StorageExchangeSegmentMaster exchSeg = commonUtil.findByInterOperabilityFlag();			
			String interOpExchCode = exchSeg.getExchangeCode();			
			StorageExchangeSegmentMaster cashExchDet = commonUtil.findByExchangeCode("CAS");
			Long interOpBusLineNo = cashExchDet.getBusinessLineNo();
			
			if(StringUtils.isNotBlank(interOpExchCode))
			{
				String convExchCode = interOpExchCode.equals("NSE") ? "BSE" : "NSE";			
				String setlNo = "";
				
				List<StorageSettlementMaster> setlList = this.setlRepo.getInterOpSetlDet(interOpExchCode,convExchCode,file_date,"Auction");
								
				List<String> uniqueSettlement = 
						setlList.stream()
					              .map(StorageSettlementMaster::getSettlementNo).distinct()
					              .collect(Collectors.toList());
				if(uniqueSettlement != null && uniqueSettlement.size() > 1 )
				{
					
				}else{
					setlNo = uniqueSettlement.get(0).toString();				
					
					List<StorageTradeDetail> tradeDetails = tradeDetRepo.findByApplicableBusinessLineNoAndVocDateAndApplicableSettlementNo(interOpBusLineNo,file_date,setlNo);
					
					List<StorageTradeDetail> grpTradeDetails = this.groupingTradeSplit(interOpBusLineNo,file_date,setlNo);
					
					List<Object> objCummTradeDetail =  tradeDetRepo.cummTradeDetails(interOpBusLineNo,file_date,setlNo);
						
					List<StorageTradeDetail> cummTradeDetails =
							objCummTradeDetail.stream()
						                .map(objects -> {
						                	StorageTradeDetail cummTrades = new StorageTradeDetail();
						                	Object[] trdObj = (Object[]) objects;
						                	cummTrades.setTransactionType(trdObj[0].toString());
						                	cummTrades.setApplicableBusinessLineNo(Long.valueOf(trdObj[1].toString()));
						                	cummTrades.setVocDate((Date) trdObj[2]);
						                	cummTrades.setUccCode(trdObj[3].toString());
						                	cummTrades.setScripCode(Long.valueOf(trdObj[4].toString()));
						                	cummTrades.setApplicableSettlementNo(trdObj[5].toString());
						                	cummTrades.setApplicableSettlementType(trdObj[6].toString());
						                	cummTrades.setClientCode(trdObj[7].toString());
						                	cummTrades.setTradeNo(trdObj[8].toString());
						                	cummTrades.setOrderNo(trdObj[9].toString());						                	
						                	cummTrades.setTradedQuantity(Long.valueOf(trdObj[10].toString()));
						                	cummTrades.setCummTradedQty(Long.valueOf(trdObj[11].toString()));
						                    return cummTrades;
						                })
						                .collect(Collectors.toList());
									
					cummTradeDetails.forEach(elem -> {
						StorageTradeDetail tradesFound = grpTradeDetails.stream().filter(v1 -> (
							
								   v1.getApplicableBusinessLineNo().equals(elem.getApplicableBusinessLineNo())
								&& v1.getVocDate().equals(elem.getVocDate())
								&& v1.getApplicableSettlementNo().equals(elem.getApplicableSettlementNo())
								&& v1.getApplicableSettlementType().equals(elem.getApplicableSettlementType())
								&& v1.getClientCode().equals(elem.getClientCode())
								&& v1.getScripCode().equals(elem.getScripCode())	
								
						)).findAny().orElse(null);   
						if(tradesFound != null)
						{
							if(elem.getTransactionType().equals("B"))
							{
						        if(tradesFound.getIntraDayBuyQuantity()>=elem.getCummTradedQty()) {
						            elem.setIntraDayBuyQuantity(elem.getTradedQuantity());
						        }else if(tradesFound.getIntraDayBuyQuantity()< elem.getCummTradedQty() 
						        	&& Long.signum((elem.getTradedQuantity() + tradesFound.getIntraDayBuyQuantity()) - elem.getCummTradedQty()) == 1 ) {						            
						        	elem.setIntraDayBuyQuantity((elem.getTradedQuantity()+tradesFound.getIntraDayBuyQuantity()) - elem.getCummTradedQty() );						        	
						        }else{
						        	elem.setIntraDayBuyQuantity(0L);
						        }
							}else if(elem.getTransactionType().equals("S")){							
						        if(tradesFound.getIntraDaySellQuantity()>=elem.getCummTradedQty()) {
						            elem.setIntraDaySellQuantity(elem.getTradedQuantity());
						        }else if(tradesFound.getIntraDaySellQuantity()< elem.getCummTradedQty() 
						        	&& Long.signum((elem.getTradedQuantity() + tradesFound.getIntraDaySellQuantity()) - elem.getCummTradedQty()) == 1 ) {						            
						        	elem.setIntraDaySellQuantity((elem.getTradedQuantity()+tradesFound.getIntraDaySellQuantity()) - elem.getCummTradedQty());						        	
						        }else{
						        	elem.setIntraDaySellQuantity(0L);						        	
						        }						        
							}
						}
				    });
					
					tradeDetails.forEach(elem -> {
						StorageTradeDetail tradesFound = cummTradeDetails.stream().filter(v1 -> (
								   v1.getApplicableBusinessLineNo().equals(elem.getApplicableBusinessLineNo())
								&& v1.getVocDate().equals(elem.getVocDate())
								&& v1.getApplicableSettlementNo().equals(elem.getApplicableSettlementNo())
								&& v1.getApplicableSettlementType().equals(elem.getApplicableSettlementType())
								&& v1.getClientCode().equals(elem.getClientCode())
								&& v1.getScripCode().equals(elem.getScripCode())	
								&& v1.getTradeNo().equals(elem.getTradeNo())
								&& v1.getOrderNo().equals(elem.getOrderNo())
						)).findAny().orElse(null);   
						if(tradesFound != null)
						{
							elem.setIntraDayBuyQuantity(tradesFound.getIntraDayBuyQuantity());
						    elem.setIntraDaySellQuantity(tradesFound.getIntraDaySellQuantity());
						}
				    });
					
					tradeDetails.forEach(elem -> {						
						if(elem.getTransactionType().equals("B"))
						{
					        if(elem.getTradedQuantity() == elem.getIntraDayBuyQuantity()) {
					            elem.setDeliveryBuyQuantity(0L);
					        }else if(elem.getTradedQuantity() > elem.getIntraDayBuyQuantity()){
					        	elem.setDeliveryBuyQuantity(elem.getTradedQuantity() - elem.getIntraDayBuyQuantity());					        	
					        }else{
					        	elem.setDeliveryBuyQuantity(0L);
					        }
						}else if(elem.getTransactionType().equals("S")){							
							if(elem.getTradedQuantity() == elem.getIntraDaySellQuantity()) {
					            elem.setDeliverySellQuantity(0L);
					        }else if(elem.getTradedQuantity() > elem.getIntraDaySellQuantity()){
					        	elem.setDeliverySellQuantity(elem.getTradedQuantity() - elem.getIntraDaySellQuantity());					        	
					        }else{
					        	elem.setDeliverySellQuantity(0L);
					        }
					        
						}
					
					});	
					
					String msg = "Trade Split Process Completed Successfully";
					tradeDetRepo.saveAll(tradeDetails)	;	
					fileLog = new FileLogWrapper();				
			    	fileLog.setInvalidRecords(0L);
					fileLog.setValidRecords(0L);			
					fileLog.setFileCode(fileMap.get("code"));
					fileLog.setTotalRecords(0L);
					fileLog.setMismatch(msg);
					fileLog.setFileDate(file_date);
					fileLog.setStatus("Successfull");
					commonUtil.writeAndGetAudit(fileLog);
					
					LOGGER.info("Error in  TradeSplitProcessService : tradeSplitProcecss:: >>"+msg);
				}				
			}			
		}catch(Exception ex){
			LOGGER.error("Error in TradeSplitProcessService : tradeSplitProcecss:: >>"+ex,ex.getMessage());
			fileLog = new FileLogWrapper();				
	    	fileLog.setInvalidRecords(0L);
			fileLog.setValidRecords(0L);			
			fileLog.setFileCode(fileMap.get("code"));
			fileLog.setTotalRecords(0L);
			fileLog.setMismatch(ex.getMessage());
			fileLog.setFileDate(file_date);
			fileLog.setStatus("Error");
			commonUtil.writeAndGetAudit(fileLog);
		}
	}
	
	public List<StorageTradeDetail> groupingTradeSplit(Long busLineNo,Date vocDate,String setlNo) throws CustomException {
		List<StorageTradeDetail> result = new ArrayList<StorageTradeDetail>();
		try {
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<StorageTradeDetail> cQuery = cBuilder.createQuery(StorageTradeDetail.class);			
			Root<StorageTradeDetail> root = cQuery.from(StorageTradeDetail.class);
		
			Predicate tbyt  = cBuilder.equal(root.get("processType"), "T-by-T");
			Predicate sme = cBuilder.equal(root.get("processType"),"SME");
			Predicate normal = cBuilder.equal(root.get("processType"),"Normal");
			Predicate notSme = cBuilder.notEqual(root.get("processType"),"SME");
			Predicate compTByTProcessType = cBuilder.or(tbyt, sme);
			Predicate compNorProcessType = cBuilder.and(normal, notSme);			
			Predicate gteBuyQty  = cBuilder.greaterThanOrEqualTo(cBuilder.sum(root.get("buyQuantity")), cBuilder.sum(root.get("sellQuantity")));
			Predicate compgteBuyQtyNorPrcsType = cBuilder.and(normal, gteBuyQty);
			Predicate compgteBuyQtyNorSmePrcsType = cBuilder.and(compNorProcessType, gteBuyQty);
			Predicate ltSellQty  = cBuilder.lessThan(cBuilder.sum(root.get("buyQuantity")), cBuilder.sum(root.get("sellQuantity")));
			Predicate compSellQtyNorPrcsType = cBuilder.and(compNorProcessType,ltSellQty);
			Predicate lteBuyQty  = cBuilder.lessThanOrEqualTo(cBuilder.sum(root.get("buyQuantity")), cBuilder.sum(root.get("sellQuantity")));
			Predicate gtBuyQty  = cBuilder.greaterThan(cBuilder.sum(root.get("buyQuantity")), cBuilder.sum(root.get("sellQuantity")));
			Predicate compltBuyQtyPrcsType = cBuilder.and(compNorProcessType, lteBuyQty);			
			Predicate complteBuyQtyNorPrcsType = cBuilder.and(normal, lteBuyQty);
			Predicate compgtBuyQtyNorProcessType = cBuilder.and(compNorProcessType, gtBuyQty);
			
			cQuery.multiselect(
					root.get("applicableBusinessLineNo"),
					root.get("vocDate"),
					root.get("uccCode"),
					root.get("clientCode"),
					root.get("scripCode"),
					root.get("applicableSettlementNo"),
					root.get("applicableSettlementType"),
					cBuilder.sum(root.get("tradedQuantity")),
					cBuilder.sum(root.get("buyQuantity")),
					cBuilder.sum(root.get("sellQuantity")),
					cBuilder.selectCase()
                     .when(compTByTProcessType, cBuilder.sum(root.get("buyQuantity")))
                     .when(compgteBuyQtyNorPrcsType, cBuilder.diff(cBuilder.sum(root.get("buyQuantity")),cBuilder.sum(root.get("sellQuantity")))).otherwise(0)
                     .alias("deliveryBuyQuantity"),
                     cBuilder.selectCase()
                     .when(compgteBuyQtyNorSmePrcsType, cBuilder.sum(root.get("sellQuantity")))
                     .when(compSellQtyNorPrcsType, cBuilder.sum(root.get("buyQuantity"))).otherwise(0)
                     .alias("intraDayBuyQuantity"),  
                     cBuilder.selectCase()
                     .when(compTByTProcessType, cBuilder.sum(root.get("sellQuantity")))
                     .when(complteBuyQtyNorPrcsType, cBuilder.diff(cBuilder.sum(root.get("sellQuantity")),cBuilder.sum(root.get("buyQuantity")))).otherwise(0).alias("deliverySellQuantity") ,
                     cBuilder.selectCase()
                     .when(compltBuyQtyPrcsType, cBuilder.sum(root.get("buyQuantity")))
                     .when(compgtBuyQtyNorProcessType, cBuilder.sum(root.get("sellQuantity"))).otherwise(0).alias("intraDaySellQuantity")                     
					)
			.where(cBuilder.and(cBuilder.equal(root.get("applicableBusinessLineNo"), busLineNo),
			       cBuilder.equal(root.get("vocDate"), vocDate),
			       cBuilder.equal(root.get("applicableSettlementNo"), setlNo),	
			       cBuilder.equal(root.get("contractGenerateStatus"), "N")	
				   ))
			.groupBy(root.get("applicableBusinessLineNo"),root.get("uccCode"),root.get("clientCode"),root.get("scripCode"),root.get("applicableSettlementNo"),root.get("applicableSettlementType"),root.get("vocDate"),root.get("processType"));
			
			result = entityManager.createQuery(cQuery).getResultList();
			
		} catch(Exception ex) {
			LOGGER.error("Error in TradeSplitProcessService : groupingTradeSplit:: >>"+ex,ex.getMessage());
			throw new CustomException(ex.getMessage());
		}		
		return result;
	}
}
