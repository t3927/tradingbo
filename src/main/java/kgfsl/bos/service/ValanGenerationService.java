package kgfsl.bos.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kgfsl.bos.constant.CommonConstants;
import kgfsl.bos.entity.mysql.StorageCommonTaxMaster;
import kgfsl.bos.entity.mysql.StorageExchangeSegmentMaster;
import kgfsl.bos.entity.mysql.StorageSeriesMaster;
import kgfsl.bos.entity.mysql.StorageSettlementMaster;
import kgfsl.bos.entity.mysql.StorageTradeChargesDetail;
import kgfsl.bos.entity.mysql.StorageTradeDetail;
import kgfsl.bos.repository.mysql.CommonTaxMasterRepository;
import kgfsl.bos.repository.mysql.SeriesMasterRepository;
import kgfsl.bos.repository.mysql.SettlementMasterRepository;
import kgfsl.bos.repository.mysql.TradeChargeDetailRepository;
import kgfsl.bos.repository.mysql.TradeDetailRepository;
import kgfsl.bos.utils.CommonMethods;
import kgfsl.bos.wrapper.FileLogWrapper;

@Service
public class ValanGenerationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValanGenerationService.class);	
	

	@PersistenceContext(unitName = "mysqlEntityManager")
	private EntityManager entityManager;		
	
	
	@Autowired
	TradeChargeDetailRepository tradeChargerepo;
	
	@Autowired
	SeriesMasterRepository seriesMastRepo;
	
	@Autowired
	SettlementMasterRepository setlRepo;
	
	@Autowired
	TradeDetailRepository tradeDetRepo;
	
	@Autowired
	CommonTaxMasterRepository commonTaxRepo;
	
	@Autowired
	TradeChargeDetailRepository tradeChargesRepo;
	
	@Autowired
	CommonMethods commonUtil;
	
	public void valanGenerationProcess(Map<String, String> fileMap) throws IOException, Exception
	{
		FileLogWrapper fileLog = new FileLogWrapper();
		DateFormat dateHis = new SimpleDateFormat("dd-MM-yyyy");
		Date file_date = null;
		try{
			Date strDate = dateHis.parse(fileMap.get("date"));	
			file_date = CommonMethods.dateConvertion(strDate);	
			Long bussLineNo = Long.parseLong(fileMap.get("businessLineNo"));
			
			StorageExchangeSegmentMaster exchSeg = commonUtil.findByInterOperabilityFlag();			
			String interOpExchCode = exchSeg.getExchangeCode();	
			
			StorageExchangeSegmentMaster cashExchDet = commonUtil.findByExchangeCode("CAS");
			Long interOpBusLineNo = cashExchDet.getBusinessLineNo();
			
			String convExchCode = interOpExchCode.equals("NSE") ? "BSE" : "NSE";		
			
			List<StorageSettlementMaster> setlList = this.setlRepo.getInterOpSetlDet(interOpExchCode,convExchCode,file_date,"Auction");
			
			List<String> uniqueSettlement = 
					setlList.stream()
				              .map(StorageSettlementMaster::getSettlementNo).distinct()
				              .collect(Collectors.toList());
			String setlNo = uniqueSettlement.get(0).toString();				
			
			List<StorageTradeDetail> tradeDetails = tradeDetRepo.findByApplicableBusinessLineNoAndVocDateAndApplicableSettlementNo(interOpBusLineNo,file_date,setlNo);
			
			List<StorageCommonTaxMaster> commtaxMasterList = commonTaxRepo.findByBusinessLineNoAndTransactionType(bussLineNo, "STAMP");
			
			List<StorageTradeChargesDetail> stampDutyCalc = this.stampDutyCalc(interOpBusLineNo,file_date,setlNo,tradeDetails,commtaxMasterList);
			
		
		}catch(Exception ex){
			LOGGER.error("Error in ValanGenerationService : valanGenerationProcess:: >>"+ex,ex.getMessage());
		}
		
	}
	
	private List<StorageTradeChargesDetail> stampDutyCalc(Long busLineNo,Date vocDate,String setlNo,List<StorageTradeDetail> tradeDetails,List<StorageCommonTaxMaster> commtaxMasterList)
	{
		try{
			List<StorageTradeChargesDetail> trdChrgDet = new ArrayList<StorageTradeChargesDetail>();
			List<StorageSeriesMaster> seriesList = seriesMastRepo.findByBusinessLineNoAndDebentureFlag(busLineNo, "Y");
			tradeDetails.forEach(elem -> {
				 Double deliveryPercent = 0.00,intradayPercent= 0.00,
				 intrdayMinimumMultiplier= 0.00,deliveryMinimumMultiplier= 0.00,forTurnover= 0.00 ,maxValue= 0.00;
				 String roundOff = "";
				 String decimals = "";
				 Date effectiveFromDate = null;
				 Date effectiveToDate = null;
				 String exemptStatus = "";
				 String gstFlag = "";
				 String defaultFlag = "";
				 Double deliveryBuyValue = elem.getDeliveryBuyQuantity() * elem.getTradedPrice();
				 Double deliverySellValue = elem.getDeliverySellQuantity() * elem.getTradedPrice();
				 Double intraDayBuyValue = elem.getIntraDayBuyQuantity() * elem.getTradedPrice();						 
				 Double intraDaySellValue = elem.getIntraDaySellQuantity() * elem.getTradedPrice();
				 byte[] taxDet = null;
				 String strTaxInfo = "";
				 JSONArray array;
				 StorageCommonTaxMaster commtaxMaster = commtaxMasterList.stream().filter(v1 -> (v1.getTransactionSubType().equals(elem.getClientState()) && v1.getDefaultFlag().equals("N") )).findAny().orElse(null);               
		        if(commtaxMaster != null) {
		        	taxDet = commtaxMaster.getTaxDetail();
		        	strTaxInfo = new String(taxDet, StandardCharsets.UTF_8);		        	
					try {
						 array = new JSONArray(strTaxInfo);
						 JSONObject jsonObj  = array.getJSONObject(0);
						 
						 intrdayMinimumMultiplier = Double.parseDouble(jsonObj.optString("intrdayMinimumMultiplier"));
						 deliveryMinimumMultiplier = Double.parseDouble(jsonObj.optString("deliveryMinimumMultiplier"));
						 forTurnover = Double.parseDouble(jsonObj.optString("forTurnover"));
						 maxValue = Double.parseDouble(jsonObj.optString("maxValue"));
						 roundOff = jsonObj.optString("roundOff");
						 decimals = jsonObj.optString("decimals");
						 effectiveFromDate = StringUtils.isNotBlank(jsonObj.optString("effectiveFromDate")) ? CommonConstants.YYYY_MM_DD.parse(jsonObj.optString("effectiveFromDate")): null;
						 effectiveToDate = StringUtils.isNotBlank(jsonObj.optString("effectiveToDate")) ? CommonConstants.YYYY_MM_DD.parse(jsonObj.optString("effectiveToDate")): new Date();
						 exemptStatus = jsonObj.optString("exemptStatus");
						 gstFlag = jsonObj.optString("gstFlag");
						 defaultFlag = jsonObj.optString("defaultFlag");
						 deliveryBuyValue = elem.getDeliveryBuyQuantity() * elem.getTradedPrice();
						 deliverySellValue = elem.getDeliverySellQuantity() * elem.getTradedPrice();
						 intraDayBuyValue = elem.getIntraDayBuyQuantity() * elem.getTradedPrice();						 
						 intraDaySellValue = elem.getIntraDaySellQuantity() * elem.getTradedPrice();
						 if(effectiveFromDate.compareTo(vocDate) <= 0 && (effectiveToDate.compareTo(vocDate) >= 0 || effectiveToDate == null)){
							 StorageSeriesMaster seriesMastFound = seriesList.stream().filter(v1 -> (v1.getSeriesCode().equals(elem.getSeriesCode()))).findAny().orElse(null);
							 if(seriesMastFound != null){
								 deliveryPercent = Double.parseDouble(jsonObj.optString("debenturesPercent"));
								 intradayPercent = Double.parseDouble(jsonObj.optString("debenturesPercent"));
									
							 }else{
								 deliveryPercent = Double.parseDouble(jsonObj.optString("deliveryPercent"));
								 intradayPercent = Double.parseDouble(jsonObj.optString("intradayPercent"));
							 }
						        
						 }						 
						 
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
		        	
		        	 }else{
		        		 StorageCommonTaxMaster commtaxDefaultMaster = commtaxMasterList.stream().filter(v1 -> (v1.getTransactionSubType().equals(elem.getClientState()) && v1.getDefaultFlag().equals("Y"))).findAny().orElse(null);               
		        		 taxDet = commtaxDefaultMaster.getTaxDetail();
				         strTaxInfo = new String(taxDet, StandardCharsets.UTF_8);		        	
							try {
								 array = new JSONArray(strTaxInfo);
								 JSONObject jsonObj  = array.getJSONObject(0);
								 
								 intrdayMinimumMultiplier = Double.parseDouble(jsonObj.optString("intrdayMinimumMultiplier"));
								 deliveryMinimumMultiplier = Double.parseDouble(jsonObj.optString("deliveryMinimumMultiplier"));
								 forTurnover = Double.parseDouble(jsonObj.optString("forTurnover"));
								 maxValue = Double.parseDouble(jsonObj.optString("maxValue"));
								 roundOff = jsonObj.optString("roundOff");
								 decimals = jsonObj.optString("decimals");
								 effectiveFromDate = jsonObj.optString("effectiveFromDate") != null ? CommonMethods.convertyyyyMMdd(jsonObj.optString("effectiveFromDate")): null;
								 effectiveToDate = jsonObj.optString("effectiveToDate") != null ? CommonMethods.convertyyyyMMdd(jsonObj.optString("effectiveToDate")): null;
								 exemptStatus = jsonObj.optString("exemptStatus");
								 gstFlag = jsonObj.optString("gstFlag");
								 defaultFlag = jsonObj.optString("defaultFlag");
								 deliveryBuyValue = elem.getDeliveryBuyQuantity() * elem.getTradedPrice();
								 deliverySellValue = elem.getDeliverySellQuantity() * elem.getTradedPrice();
								 intraDayBuyValue = elem.getIntraDayBuyQuantity() * elem.getTradedPrice();						 
								 intraDaySellValue = elem.getIntraDaySellQuantity() * elem.getTradedPrice();
								 if(effectiveFromDate.compareTo(vocDate) <= 0 && (effectiveToDate.compareTo(vocDate) >= 0 || effectiveToDate == null)){
									 StorageSeriesMaster seriesMastFound = seriesList.stream().filter(v1 -> (v1.getSeriesCode().equals(elem.getSeriesCode()))).findAny().orElse(null);
									 if(seriesMastFound != null){
										 deliveryPercent = Double.parseDouble(jsonObj.optString("debenturesPercent"));
										 intradayPercent = Double.parseDouble(jsonObj.optString("debenturesPercent"));
											
									 }else{
										 deliveryPercent = Double.parseDouble(jsonObj.optString("deliveryPercent"));
										 intradayPercent = Double.parseDouble(jsonObj.optString("intradayPercent"));
									 }  
								 }						 
								 
								} catch (Exception e) {
									e.printStackTrace();
								} 
		        	 }
		        StorageTradeChargesDetail trdChrage = new StorageTradeChargesDetail();
		        
		        trdChrage.setBusinessLineNo(elem.getApplicableBusinessLineNo());
		        trdChrage.setVocDate(elem.getVocDate());
		        trdChrage.setClientCode(elem.getClientCode());
		        trdChrage.setSymbol(elem.getSymbol());
		        trdChrage.setSeriesCode(elem.getSeriesCode());
		        trdChrage.setSettlementNo(elem.getApplicableSettlementNo());
		        trdChrage.setSettlementType(elem.getApplicableSettlementType());
		        trdChrage.setTradeNo(elem.getTradeNo());
		        trdChrage.setOrderNo(elem.getOrderNo());
		        trdChrage.setDlyBuyValue(deliveryBuyValue);
		        trdChrage.setDlySellValue(deliverySellValue);
		        trdChrage.setTrdBuyValue(intraDayBuyValue);
		        trdChrage.setTrdSellValue(intraDaySellValue);
		        if(exemptStatus.equals("0")){
		        	Double deliveryStamp = deliveryBuyValue >0 ? deliveryBuyValue * (deliveryPercent/100) : 0;
        			Double intraDayStamp = intraDayBuyValue >0 ? intraDayBuyValue * (intradayPercent/100) : 0;
		        	
        			String strStampDuty = StringUtils.equals(decimals, "T") ? 
    		        		CommonConstants.TWO_DECIMAL.format(deliveryStamp + intraDayStamp) :
    			        		CommonConstants.FOUR_DECIMAL.format(deliveryStamp + intraDayStamp);
		        	trdChrage.setStampDuty(Double.parseDouble(strStampDuty));
		        	
		        	trdChrgDet.add(trdChrage);
		        	
		        }
		     
		    });
			
			Map<StorageTradeChargesDetail, List<StorageTradeChargesDetail>> ClientMap = 
					trdChrgDet.stream()
				    .collect(Collectors.groupingBy(StorageTradeChargesDetail::getClientCode))
				    .entrySet().stream()
				    .collect(Collectors.toMap(e -> e.getValue().stream()
				        .collect(Collectors.reducing(
				            (l, r) -> new StorageTradeChargesDetail(l.getClientCode(),l.getSymbol(),l.getSeriesCode() ,l.getStampDuty() + r.getStampDuty())))
				         .get(), e -> e.getValue()));
			
			
					
			tradeChargesRepo.saveAll(trdChrgDet);
					    
		}catch(Exception ex){
			LOGGER.error("Error in ValanGenerationService : valanGenerationProcess:: >>"+ex,ex.getMessage());
		}
		return null;
		
	}
	
	public List<StorageTradeDetail> getTradeDetails(Long busLineNo,Date vocDate,String setlNo) {
			List<StorageTradeDetail> result = new ArrayList<StorageTradeDetail>();
		final List<Predicate> predicates = new ArrayList<>();
		try {
			CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<StorageTradeDetail> cQuery = cBuilder.createQuery(StorageTradeDetail.class);
			
			Root<StorageTradeDetail> tradeRoot = cQuery.from(StorageTradeDetail.class);
			
			Root<StorageSeriesMaster> seriesRoot = cQuery.from(StorageSeriesMaster.class);
			Join<StorageTradeDetail, StorageSeriesMaster> clntMasJoin = tradeRoot.join("seriesMaster", JoinType.LEFT);
			
			
			cQuery.multiselect(
					cBuilder.prod(tradeRoot.get("deliveryBuyQuantity") , tradeRoot.get("tradedPrice")).alias("deliveryBuyTradeValue"),
					cBuilder.prod(tradeRoot.get("deliverySellQuantity") , tradeRoot.get("tradedPrice")).alias("deliverySellTradeValue"),
					cBuilder.prod(tradeRoot.get("intraDayBuyQuantity") , tradeRoot.get("tradedPrice")).alias("intraDayBuyTradeValue"),
					cBuilder.prod(tradeRoot.get("intraDaySellQuantity") , tradeRoot.get("tradedPrice")).alias("intraDaySellTradeValue"),
					tradeRoot.get("applicableBusinessLineNo"),
					tradeRoot.get("vocDate"),
					tradeRoot.get("clientCode"),
					tradeRoot.get("scripCode"),
					tradeRoot.get("applicableSettlementNo"),
					tradeRoot.get("applicableSettlementType"),
					tradeRoot.get("transactionType"),
					tradeRoot.get("id"),
					seriesRoot.get("debentureFlag").alias("debentureFlag")
                     
					).where(cBuilder.or(cBuilder.equal(tradeRoot.get("seriesCode"),seriesRoot.get("seriesCode")),
									    cBuilder.equal(tradeRoot.get("businessLineNo"),seriesRoot.get("businessLineNo")),
									    cBuilder.equal(cBuilder.isNull(seriesRoot.get("debentureFlag")),"Y")),
						   cBuilder.and(cBuilder.equal(tradeRoot.get("applicableBusinessLineNo"), busLineNo),
								   		cBuilder.equal(tradeRoot.get("vocDate"), vocDate),
								   		cBuilder.equal(tradeRoot.get("applicableSettlementNo"), setlNo),	
								   		cBuilder.equal(tradeRoot.get("contractGenerateStatus"), "N")
				   ));
			
			result = entityManager.createQuery(cQuery).getResultList();
			
		} catch(Exception ex) {
			LOGGER.error("Error in ValanGenerationService : groupingTradeSplit:: >>"+ex,ex.getMessage());
			return  null;
		}
		
		return result;
	}
	
	
}
