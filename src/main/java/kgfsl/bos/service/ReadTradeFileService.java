package kgfsl.bos.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.mockito.internal.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import kgfsl.bos.entity.clickhouse.TempTradeDetails;
import kgfsl.bos.entity.mysql.StorageClientBrokerageScheme;
import kgfsl.bos.entity.mysql.StorageClientBusinessLineMapping;
import kgfsl.bos.entity.mysql.StorageClientMaster;
import kgfsl.bos.entity.mysql.StorageExchangeSegmentMaster;
import kgfsl.bos.entity.mysql.StorageFileConfig;
import kgfsl.bos.entity.mysql.StorageScripDetail;
import kgfsl.bos.entity.mysql.StorageSeriesMaster;
import kgfsl.bos.entity.mysql.StorageSettlementMaster;
import kgfsl.bos.entity.mysql.StorageSettlementType;
import kgfsl.bos.entity.mysql.StorageTradeDetail;
import kgfsl.bos.mapper.TempTradeDetailsMapper;
import kgfsl.bos.repository.mysql.ClientBrokerageSchemeRepository;
import kgfsl.bos.repository.mysql.ClientBusinessLineMappingRepository;
import kgfsl.bos.repository.mysql.ClientMasterRepository;
import kgfsl.bos.repository.mysql.FileConfigRepository;
import kgfsl.bos.repository.mysql.ScripDetailRepository;
import kgfsl.bos.repository.mysql.SeriesMasterRepository;
import kgfsl.bos.repository.mysql.SettlementMasterRepository;
import kgfsl.bos.repository.mysql.SettlementTypeRepository;
import kgfsl.bos.repository.mysql.TradeDetailRepository;
import kgfsl.bos.utils.CommonMethods;
import kgfsl.bos.utils.CustomException;
import kgfsl.bos.wrapper.FileLogWrapper;

@Service
public class ReadTradeFileService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReadTradeFileService.class);	
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate; 
	
	@Autowired
	SettlementMasterRepository setlRepo;
	
	@Autowired
	ScripDetailRepository scripDtlRepo;
	
	@Autowired
	ClientMasterRepository clientMasterRepo;
	
	@Autowired
	ClientBrokerageSchemeRepository clntBrokRepo;
	
	@Autowired
	ClientBusinessLineMappingRepository clntBusLnMapRepo;
	
	@Autowired
	FileConfigRepository fileRepo;
	
	@Autowired
	SeriesMasterRepository seriesMasterRepo;
	
	@Autowired
	SettlementTypeRepository setlTypeRepo;
	
	@Resource
    private TempTradeDetailsMapper tempTradeMapper;	
	
	@Autowired
	CommonMethods commMeth;
		
	@Autowired
	TradeDetailRepository tradeDetailRepo;
	
	public void sendToClickhouse(String fileContent,Map<String, String> fileMap) throws IOException, Exception
	{ 
		Date vocDate = null;
		FileLogWrapper fileLog = new FileLogWrapper();
		try{	
			vocDate = CommonMethods.convertyyyyMMdd(fileMap.get("date"));
			commMeth.fileUploadAuditSave("Progress",fileMap.get("code"),vocDate);	
			
			fileLog.setFileCode(fileMap.get("code"));	
			fileLog.setFileDate(vocDate);
					
			String busLineNo = fileMap.get("businessLineNo");
			String batchId = CommonMethods.getSessionId();
			String fileCode = fileMap.get("code");
			Long bussLineNo = Long.parseLong(busLineNo);
			
			/**Append Additional Values**/
			String appendValue = ","+fileMap.get("date").toString()+"," + busLineNo +"," + batchId;
			List<String> trdList = new ArrayList<String>(Arrays.asList(fileContent.split("\n")));
			trdList.replaceAll(x -> x.replace("\r", ""));
			ArrayList<String> apndList = new ArrayList<>(trdList);
			apndList.replaceAll(x -> x + appendValue + "\r\n" );
			
			String fileDet = apndList.stream().map(e -> e.toString()).reduce("", String::concat);
			
			StorageFileConfig fileConfig =  this.fileRepo.findByFileCode(fileCode);
			if (trdList.size() > 0) {
				String[] fileColumnDet = trdList.get(0).split(",");	
				Long configColumnCount = fileConfig.getColumnCount();
				Long fileColumnCount = (long) fileColumnDet.length;
				if (fileColumnCount != configColumnCount) {
					throw new CustomException("Columns Count mismatch!. Actual:'" + fileColumnCount + "', expected:'" + configColumnCount + "''");
				}
			} 
			
			/** Settlement No & Settlement Date **/
			
			List<StorageSettlementMaster> setlList = this.setlRepo.findByTradeFromDateAndBusinessLineNo(vocDate, bussLineNo);
			if(setlList == null || setlList.size() == 0)
			{
				throw new CustomException("Settlement Details are not available in settlement master");
			}			
					
			kafkaTemplate.send("MessageTopic1","TradeDetails",fileDet);		
			LOGGER.info("Error in ReadTradeFileService : sendToClickhouse:: >> Trade File Details Updated Successfully into Clickhouse");
			
			List<TempTradeDetails> tradeDetails = tempTradeMapper.selectTmpTradeList();
			this.tradeFileValidation(tradeDetails,setlList,fileConfig,bussLineNo,vocDate,fileCode);
			
		
		
		}catch(Exception ex){
			LOGGER.error("Error in ReadTradeFileService : sendToClickhouse:: >>"+ex,ex.getMessage());
			fileLog.setFileCode(fileMap.get("code"));
			fileLog.setError(ex.getMessage());
			fileLog.setFileDate(vocDate);
			fileLog.setStatus("Error");
		}finally {
			commMeth.writeAndGetAudit(fileLog);
		}
	}

	
	public void tradeFileValidation(List<TempTradeDetails> tempTradeList,List<StorageSettlementMaster> setlList,StorageFileConfig fileConfig,Long bussLineNo,Date file_date, String fileCode) throws CustomException
	{
		 FileLogWrapper fileLog = new FileLogWrapper();
		try{			
			
			StorageExchangeSegmentMaster exchSeg = commMeth.findByInterOperabilityFlag();
			Long interOpBusLineNo = exchSeg.getBusinessLineNo();
			
			StorageExchangeSegmentMaster cashExchDet = commMeth.findByExchangeCode("CAS");		
		
			/** Scrip Master **/			
			List<String> symbolList = tempTradeList.stream()
	                .map(TempTradeDetails::getSymbol)
	                .filter(Objects::nonNull)
	                .map(symbol -> symbol.split(",")) 
	                .flatMap(Arrays::stream) 
	                .map(String::toUpperCase)
	                .distinct()
	                .collect(Collectors.toList());
			
			List<StorageScripDetail> scripDtl =  scripDtlRepo.findByBusinessLineNoAndSymbolInAndEffToDateIsNull(bussLineNo,symbolList);
			
			List<StorageSettlementType> setlTypeList = setlTypeRepo.findbyBusinessLineNoAndProcessTypeNot(interOpBusLineNo,"Auction");
								
			tempTradeList.forEach(elem -> {
				StorageScripDetail scripFound = scripDtl.stream().filter(v1 -> (v1.getSymbol().equals(elem.getSymbol()) && v1.getSeriesCode().equals(elem.getSeries()) )).findAny().orElse(null);               
		        if(scripFound != null) {
		            elem.setScripCode(scripFound.getScrip().getScripCode());
		            if(bussLineNo.equals(interOpBusLineNo))
					{
			            elem.setApplicableSymbol(elem.getSymbol());
			            elem.setApplicableSeries(elem.getSeries());
					}
		        }else{
		        	elem.setRemarks("Invalid scrip details - " + elem.getSymbol() +"," );
		        
		        }
		    });			
			/**Settlement Type**/			
			List<String> seriesCodeList = tempTradeList.stream()
	                .map(TempTradeDetails::getSeries)
	                .filter(Objects::nonNull)
	                .map(seriesCode -> seriesCode.split(",")) 
	                .flatMap(Arrays::stream) 
	                .map(String::toUpperCase)
	                .distinct()
	                .collect(Collectors.toList());
			
			List<StorageSeriesMaster> seriesList = seriesMasterRepo.findByBusinessLineNoAndSeriesCode(bussLineNo,seriesCodeList);			
			
			tempTradeList.forEach(elem -> {
				StorageSeriesMaster seriesFound = seriesList.stream().filter(v1 -> (v1.getSeriesCode().equals(elem.getSeries()))).findAny().orElse(null);               
		        if(seriesFound != null) {
		            elem.setSettlementType(seriesFound.getSetlType());
		        }else{
		        	elem.setRemarks(elem.getRemarks() + " Settlement Type Mismatch - " + elem.getSeries() +",");
		        }
		    });
			
			/** Settlement No & Settlement Date **/			
			tempTradeList.forEach(elem -> {
				StorageSettlementMaster setlFound = setlList.stream().filter(v1 -> (v1.getSettlementType().equals(elem.getSettlementType()))).findAny().orElse(null);               
		        if(setlFound != null) {
		            elem.setSettlementNo(setlFound.getSettlementNo());
		            elem.setSettlementDate(setlFound.getPayInDate());
		            elem.setVocDate(setlFound.getTradeFromDate());
		            elem.setBusinessLineNo(bussLineNo);
		            elem.setApplicableBusinessLineNo(cashExchDet.getBusinessLineNo());		
		            elem.setOrderDateTime(CommonMethods.convert(elem.getTradeActyDateTime()));
		            elem.setTradeDateTime(CommonMethods.convert(elem.getTradeActyDateTime()));	
		            
		            if (elem.getTransactionType() != null && elem.getTransactionType().equals("2")){
		            	elem.setTransactionType("S");
		    		}else if (elem.getTransactionType() != null && elem.getTransactionType().equals("1")){
		            	elem.setTransactionType("B");
		    		}
		            
		            if(bussLineNo.equals(interOpBusLineNo))
					{
		            	elem.setApplicableSettlementNo(elem.getSettlementNo());
			            elem.setApplicableSettlementType(elem.getSettlementType());	
			        }
		        }else{
		        	elem.setRemarks(elem.getRemarks() + "Settlement Details not found - " + elem.getSettlementType() + ",");
		        }
		    });
			
			/** Inter Operbale Settlement Type Updation **/
			tempTradeList.forEach(elem -> {
				StorageSettlementType setlTypeFound = setlTypeList.stream().filter(v1 -> (v1.getSettlementType().equals(elem.getSettlementType()))).findAny().orElse(null);               
	        if(setlTypeFound != null) {
	            elem.setApplicableSettlementType(!bussLineNo.equals(interOpBusLineNo) ? setlTypeFound.getSettlementType() : elem.getSettlementType());	
	            elem.setProcessType(setlTypeFound.getProcessType());
	        }
			});	
			
			/** Client Master **/			
			List<String> clientList = tempTradeList.stream()
	                .map(TempTradeDetails::getTerminalCode)
	                .filter(Objects::nonNull)
	                .map(terminalCode -> terminalCode.split(",")) 
	                .flatMap(Arrays::stream) 
	                .map(String::toUpperCase)
	                .distinct()
	                .collect(Collectors.toList());
			
			List<StorageClientMaster> clientDtl =  clientMasterRepo.findByUccCode(clientList);			
			
			tempTradeList.forEach(elem -> {
				StorageClientMaster clientFound = clientDtl.stream().filter(v1 -> (v1.getUccCode().equals(elem.getTerminalCode()))).findAny().orElse(null);               
		        if(clientFound != null) {
		            elem.setClientCode(clientFound.getClientCode());
		            elem.setClientState(clientFound.getClientStateCode());
		        }else{
		        	elem.setRemarks(elem.getRemarks() +"Client Account Mismatch - " + elem.getTerminalCode() +",");
		        }
		    });
			
			/** Client Business Line Mapping **/			
			List<String> internalClientCodeList = tempTradeList.stream()
	                .map(TempTradeDetails::getClientCode)
	                .filter(Objects::nonNull)
	                .map(clientCode -> clientCode.split(",")) 
	                .flatMap(Arrays::stream) 
	                .map(String::toUpperCase)
	                .distinct()
	                .collect(Collectors.toList());
			
			List<StorageClientBusinessLineMapping> clientMapDtl =  clntBusLnMapRepo.findByBusinessLineNoAndClientCode(bussLineNo,internalClientCodeList);			
			
			tempTradeList.forEach(elem -> {
				StorageClientBusinessLineMapping clientFound = clientMapDtl.stream().filter(v1 -> (v1.getClientCode().equals(elem.getClientCode()))).findAny().orElse(null);               
		        if(clientFound != null) {
		            elem.setBusLineMapping(true);
		        }else{
		        	elem.setRemarks(elem.getRemarks() +"Exchange Segment Mapping not found - " + elem.getTerminalCode() +",");
		        }
		    });
			
			/** Client Brokerage Scheme **/			
			if( internalClientCodeList!= null && internalClientCodeList.size() > 0){
				List<StorageClientBrokerageScheme> clntBrokSchemeList = clntBrokRepo.findByBusinessLineNoAndClientCode(bussLineNo, internalClientCodeList);			
				
				tempTradeList.forEach(elem -> {
					StorageClientBrokerageScheme brokFound = clntBrokSchemeList.stream().filter(v1 -> (v1.getClientCode().equals(elem.getClientCode()))).findAny().orElse(null);               
			        if(brokFound != null) {
			            elem.setEqDeliveryScheme(brokFound.getEqDeliverySchemeId());
			            elem.setEqTradingScheme(brokFound.getEqTradingSchemeId());
			        }else{
			        	elem.setRemarks(elem.getRemarks() +"Brokerage Scheme not mapped - " + elem.getTerminalCode() +",");
			        }
			    });
			}	
			
			tempTradeList.stream()
				        .map(s-> {
				        	TempTradeDetails n = new TempTradeDetails(); 
				                     n.setCreatedAt(new Date());
				                     return n; 
				                 });			
		
			List<String> remarksList = tempTradeList.stream()
	                .map(TempTradeDetails::getRemarks)
	                .filter(Objects::nonNull)
	                .map(remarks -> remarks.split(",")) 
	                .flatMap(Arrays::stream) 
	                .map(String::toUpperCase)
	                .distinct()
	                .collect(Collectors.toList());
			
			String mimatchInfo = remarksList.toString();
			
			Long errorCount = tempTradeList.stream().filter(e -> StringUtils.isEmpty(e.getRemarks())).count();
			
			Long totalRecordCount = tempTradeList.stream().count();
			Long successCount = (totalRecordCount - errorCount);
			
			fileLog.setInvalidRecords(errorCount);
			fileLog.setValidRecords(successCount);			
			fileLog.setFileCode(fileCode);
			fileLog.setTotalRecords(totalRecordCount);
			fileLog.setMismatch(mimatchInfo);
			fileLog.setFileDate(file_date);
			if(fileConfig.equals("F") && errorCount > 0)				
				fileLog.setStatus("Mismatch");
			else{			
				fileLog.setStatus("Successfull");
				
				List<StorageTradeDetail> trdDetList =  this.mapingTempTradeDetail(tempTradeList);
				tradeDetailRepo.saveAll(trdDetList);
			}
			
			commMeth.writeAndGetAudit(fileLog);
			
		}catch(Exception ex){
			LOGGER.error("Error in ReadTradeFileService : tradeFileValidation:: >>"+ex,ex.getMessage());
			throw new CustomException(ex.getMessage());
		}
		LOGGER.info("Error in ReadTradeFileService : tradeFileValidation:: >> Trade Validation Completed Successfully");
	}
	

	private List<StorageTradeDetail> mapingTempTradeDetail(List<TempTradeDetails> tempTradeList){
	    List<StorageTradeDetail> trdDetList = new ArrayList<StorageTradeDetail>();
	    tempTradeList.forEach(elem -> {
			StorageTradeDetail trdDet = new StorageTradeDetail();
			if(StringUtils.isEmpty(elem.getRemarks())) {
				trdDet.setBusinessLineNo(elem.getBusinessLineNo());
				trdDet.setVocDate(elem.getVocDate());
				trdDet.setClientCode(elem.getClientCode());
				trdDet.setUccCode(elem.getTerminalCode());
				trdDet.setScripCode(elem.getScripCode());
				trdDet.setSymbol(elem.getSymbol());
				trdDet.setSeriesCode(elem.getSeries());
				trdDet.setApplicableSymbol(elem.getApplicableSymbol());
				trdDet.setApplicableSeries(elem.getApplicableSeries());
				trdDet.setApplicableSettlementNo(elem.getApplicableSettlementNo());
				trdDet.setApplicableSettlementType(elem.getApplicableSettlementType());
				trdDet.setApplicableBusinessLineNo(elem.getApplicableBusinessLineNo());
				trdDet.setTradeNo(elem.getTradeNo());
				trdDet.setOrderNo(elem.getOrderNo());
				trdDet.setTradedTime(elem.getTradeActyDateTime().toString().substring(12,20));
				//trdDet.setTradeActyDateTime(elem.getTradeActyDateTime());
				trdDet.setOrderDateTime(elem.getOrderDateTime());
				trdDet.setTransactionType(elem.getTransactionType());
				trdDet.setTradedQuantity(Long.parseLong(elem.getTradedQuantity()));
				trdDet.setTradedPrice(Double.parseDouble(elem.getTradedPrice()));
				trdDet.setProprietryClient(elem.getProClient());
				trdDet.setSettlementNo(elem.getSettlementNo());
				trdDet.setSettlementType(elem.getSettlementType());
				trdDet.setSettlementDate(elem.getSettlementDate());
				trdDet.setCtclId(elem.getCtclId());
				trdDet.setBuyQuantity(elem.getBuyQty());
				trdDet.setSellQuantity(elem.getSellQty());
				trdDet.setContractGenerateStatus("N");
				trdDet.setAmendTradeIndicator("N");
				trdDet.setProcessType(elem.getProcessType());
				trdDetList.add(trdDet);
			}
		});
	return trdDetList;
	}

}
