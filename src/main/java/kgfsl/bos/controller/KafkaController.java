package kgfsl.bos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/TradeDetails/")
public class KafkaController {
	
	@Autowired
	private KafkaTemplate<String,String> kafkaTemplate; 
	
	@PostMapping("/producer")	
	public String publish()
	{
		kafkaTemplate.send("MessageTopic1","001","001,S,BAJAJCJ,EQ");
		return "Trade Details Send to Kafka Successfully";
	}

}
