package kgfsl.bos.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kgfsl.bos.service.TradeSplitProcessService;

@RestController 
@RequestMapping("/OblgGeneration")
public class TradeSplitProcessController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TradeSplitProcessController.class);
	
	@Autowired
	TradeSplitProcessService trdSplitPrcsService;	

	
	@RequestMapping(value = "/tradeSplit",method = RequestMethod.POST)
	public void readTradeFile(@RequestBody Map<String, String> fileMap) throws Exception,FileNotFoundException, IOException
	{ 	
		try{
			this.trdSplitPrcsService.tradeSplitProcecss(fileMap);		    
		}catch(Exception ex){
			LOGGER.error("Error in TradeFileUploadController : readTradeFile:: >>"+ex,ex.getMessage());
		}
	}

}
