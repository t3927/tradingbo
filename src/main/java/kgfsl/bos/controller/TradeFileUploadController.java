package kgfsl.bos.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import kgfsl.bos.service.ReadTradeFileService;
import kgfsl.bos.utils.CommonMethods;

@RestController
@RequestMapping("/tradefileprocess")
public class TradeFileUploadController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TradeFileUploadController.class);		
	
	@Autowired
	ReadTradeFileService readTradeService;	
	
	@Autowired
	CommonMethods commonUtil;
	
	@RequestMapping(value = "/upload",method = RequestMethod.POST)
	public void gcpFileUpload()
	{
		 BlobId blobId = BlobId.of("trdprcssbkt", "07072021_1234.txt");
	        BlobInfo info = BlobInfo.newBuilder(blobId).build();
	        File fileDet = new File("D:\\Cloud_Test","07072021_1234.txt");
	        try {
	            byte[] arr = Files.readAllBytes(Paths.get(fileDet.toURI()));
	             StorageOptions storageOptions = StorageOptions.newBuilder()
	                       .setProjectId("testprjtrdprcss")
	                        .setCredentials(GoogleCredentials.fromStream(new 
	                         FileInputStream("D:\\Nisha_Source\\Groww//testprjtrdprcss-f28fe1059cc8.json"))).build();
	                    Storage storage = storageOptions.getService();
	            
	            storage.create(info,arr);
	        } catch (Exception e) {          
	            e.printStackTrace();
	        }
	       System.out.println("File uplaoded Successfully"); 		
	}
	
	@RequestMapping(value = "/read",method = RequestMethod.POST)
	public void readTradeFile(@RequestBody Map<String, String> fileMap) throws Exception,FileNotFoundException, IOException
	{ 
		try
		{
		   StorageOptions storageOptions = StorageOptions.newBuilder()
                  .setProjectId("testprjtrdprcss")
                   .setCredentials(GoogleCredentials.fromStream(new 
                    FileInputStream("D:\\Nisha_Source\\Groww//testprjtrdprcss-f28fe1059cc8.json"))).build();
               Storage storage = storageOptions.getService();                
   
		    Blob blob = storage.get("trdprcssbkt", "07072021_1234.txt");
		    final String fileContent = new String(blob.getContent());
			
			
		    this.readTradeService.sendToClickhouse(fileContent,fileMap);
		    
		   
		}catch(Exception ex){
			LOGGER.error("Error in TradeFileUploadController : readTradeFile:: >>"+ex,ex.getMessage());
		}
	}

}
