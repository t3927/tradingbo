package kgfsl.bos.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import kgfsl.bos.service.ValanGenerationService;

@RestController
@RequestMapping("/valan")
public class ValanGenerationController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValanGenerationController.class);	
	
	@Autowired
	ValanGenerationService valanGenService;
	
	@RequestMapping(value = "/taxCalc",method = RequestMethod.POST)
	public void valanProcess(@RequestBody Map<String, String> fileMap) throws Exception,FileNotFoundException, IOException
	{ 	
		try{
			this.valanGenService.valanGenerationProcess(fileMap);		    
		}catch(Exception ex){
			LOGGER.error("Error in ValanGenerationController : valanProcess:: >>"+ex,ex.getMessage());
		}
	}

}
