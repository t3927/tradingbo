package kgfsl.bos.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kgfsl.bos.entity.clickhouse.TempTradeDetails;

@Mapper
public interface TempTradeDetailsMapper {
	
	  List<TempTradeDetails> selectTmpTradeList();

}
