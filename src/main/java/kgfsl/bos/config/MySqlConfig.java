package kgfsl.bos.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import kgfsl.bos.utils.CommonMethods;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "mysqlEntityManager",
        transactionManagerRef = "mysqlTransactionManager",
        basePackages = {"kgfsl.bos.repository.mysql"})
@EntityScan(basePackages = {"kgfsl.bos.entity.mysql"})
public class MySqlConfig {
	
	
	@Autowired
	private Environment env;

@Bean
@Primary
@ConfigurationProperties(prefix = "spring.datasource")
public DataSource mySqlDataSource()
{
	return DataSourceBuilder.create().build();
}
@Bean
@Primary
public Map<String, Object> jpaProperties() {
	Map<String, Object> props = new HashMap<>();
	props.put("hibernate.dialect", CommonMethods.getDialect("mysql"));
	props.put("javax.persistence.validation.mode", "none");
	props.put("hibernate.id.new_generator_mappings", "false");
	return props;
}

@Primary
@Bean
public JpaVendorAdapter jpaVendorAdapter() {
	HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
	hibernateJpaVendorAdapter.setShowSql(false);
	hibernateJpaVendorAdapter.setGenerateDdl(false);
	hibernateJpaVendorAdapter.setDatabase(CommonMethods.getJPAVendor("mysql"));
	return hibernateJpaVendorAdapter;
}

@Bean()
@Qualifier("jdbcTemplate")
public JdbcTemplate jdbcTemplate() {
	return new JdbcTemplate(this.mySqlDataSource());
}

@Primary
@Bean
public PlatformTransactionManager mysqlTransactionManager() {
	return new JpaTransactionManager(mysqlEntityManager().getObject());
}

@Primary
@Bean
public LocalContainerEntityManagerFactoryBean mysqlEntityManager() {
	LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
	lef.setDataSource(this.mySqlDataSource());
	lef.setJpaPropertyMap(this.jpaProperties());
	lef.setJpaVendorAdapter(this.jpaVendorAdapter());
	String entityPackages = "kgfsl.bos.entity.mysql";
	lef.setPackagesToScan(entityPackages.split(","));
	return lef;
}



}