package kgfsl.bos.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "kgfsl" })
@EnableAutoConfiguration(exclude = { MultipartAutoConfiguration.class })
@MapperScan(value = "kgfsl.bos.mapper")
@EnableJpaRepositories
public class BosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BosApplication.class, args);
	}
	

	


}

