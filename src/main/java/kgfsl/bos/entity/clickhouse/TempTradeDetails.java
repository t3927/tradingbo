package kgfsl.bos.entity.clickhouse;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;


public class TempTradeDetails {
	String tradeNo ;
	String tradeStatus ;
	String symbol ;
	String series ;
	String stkName ;
	String bookType ;
	String instrumentType ;
	String marketType ;
	String userId ;
	String branchNo ;
	String transactionType ;
	String tradedQuantity ;
	String tradedPrice ;
	String proClient ;
	String terminalCode ;
	String participantCode ;
	String auctionPartType ;
	String auctionNo ;
	String settlementPeriod ;
	String tradeActyDateTime ;
	String tradeModifiedDateTime;
	String orderNo ;
	String custodianParticipantId ;
	String tradeEntryModDateTime;
	String ctclId ;
	String fileDate;
	Long businessLineNo;
	String batchId;
	int createdBy ;
	Date createdAt;	
	String remarks = "";
	
	@Transient
	Long scripCode;
	
	@Transient
	String clientCode;
	
	@Transient
	String eqDeliveryScheme;
	
	@Transient
	String eqTradingScheme;
	
	@Transient
	boolean busLineMapping;
	
	@Transient
	String settlementType;
	
	@Transient
	String settlementNo;
	
	@Transient
	Date settlementDate;
	
	@Transient
	Date vocDate;
	
	@Transient
	Timestamp orderDateTime;
	
	@Transient
	Timestamp tradeDateTime;
	
	@Transient
	Date propClient;
	
	@Transient
	Long buyQty;
	
	@Transient
	Long sellQty;
	
	@Transient	
	String applicableSymbol ;
	
	@Transient	
	String applicableSeries ;
	
	@Transient	
	String applicableSettlementNo ;
	
	@Transient	
	String applicableSettlementType ;
	
	@Transient	
	Long applicableBusinessLineNo;
	
	@Transient	
	String processType;
	
	@Transient
	String clientState;
	
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public String getStkName() {
		return stkName;
	}
	public void setStkName(String stkName) {
		this.stkName = stkName;
	}
	public String getBookType() {
		return bookType;
	}
	public void setBookType(String bookType) {
		this.bookType = bookType;
	}
	public String getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}
	public String getMarketType() {
		return marketType;
	}
	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBranchNo() {
		return branchNo;
	}
	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTradedQuantity() {
		return tradedQuantity;
	}
	public void setTradedQuantity(String tradedQuantity) {
		this.tradedQuantity = tradedQuantity;
	}
	public String getTradedPrice() {
		return tradedPrice;
	}
	public void setTradedPrice(String tradedPrice) {
		this.tradedPrice = tradedPrice;
	}
	public String getProClient() {
		return proClient;
	}
	public void setProClient(String proClient) {
		this.proClient = proClient;
	}
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public String getParticipantCode() {
		return participantCode;
	}
	public void setParticipantCode(String participantCode) {
		this.participantCode = participantCode;
	}
	public String getAuctionPartType() {
		return auctionPartType;
	}
	public void setAuctionPartType(String auctionPartType) {
		this.auctionPartType = auctionPartType;
	}
	public String getAuctionNo() {
		return auctionNo;
	}
	public void setAuctionNo(String auctionNo) {
		this.auctionNo = auctionNo;
	}
	public String getSettlementPeriod() {
		return settlementPeriod;
	}
	public void setSettlementPeriod(String settlementPeriod) {
		this.settlementPeriod = settlementPeriod;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public String getTradeActyDateTime() {
		return tradeActyDateTime;
	}
	public void setTradeActyDateTime(String tradeActyDateTime) {
		this.tradeActyDateTime = tradeActyDateTime;
	}
	public String getTradeModifiedDateTime() {
		return tradeModifiedDateTime;
	}
	public void setTradeModifiedDateTime(String tradeModifiedDateTime) {
		this.tradeModifiedDateTime = tradeModifiedDateTime;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustodianParticipantId() {
		return custodianParticipantId;
	}
	public void setCustodianParticipantId(String custodianParticipantId) {
		this.custodianParticipantId = custodianParticipantId;
	}	
	public String getTradeEntryModDateTime() {
		return tradeEntryModDateTime;
	}
	public void setTradeEntryModDateTime(String tradeEntryModDateTime) {
		this.tradeEntryModDateTime = tradeEntryModDateTime;
	}
	public String getCtclId() {
		return ctclId;
	}
	public void setCtclId(String ctclId) {
		this.ctclId = ctclId;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getFileDate() {
		return fileDate;
	}
	public void setFileDate(String fileDate) {
		this.fileDate = fileDate;
	}
	public Long getBusinessLineNo() {
		return businessLineNo;
	}
	public void setBusinessLineNo(Long businessLineNo) {
		this.businessLineNo = businessLineNo;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	
	/** Tranisent **/
	public Long getScripCode() {
		return scripCode;
	}
	public void setScripCode(Long scripCode) {
		this.scripCode = scripCode;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getEqDeliveryScheme() {
		return eqDeliveryScheme;
	}
	public void setEqDeliveryScheme(String eqDeliveryScheme) {
		this.eqDeliveryScheme = eqDeliveryScheme;
	}
	public String getEqTradingScheme() {
		return eqTradingScheme;
	}
	public void setEqTradingScheme(String eqTradingScheme) {
		this.eqTradingScheme = eqTradingScheme;
	}
	public boolean isBusLineMapping() {
		return busLineMapping;
	}
	public void setBusLineMapping(boolean busLineMapping) {
		this.busLineMapping = busLineMapping;
	}
	public String getSettlementType() {
		return settlementType;
	}
	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}
	public String getSettlementNo() {
		return settlementNo;
	}
	public void setSettlementNo(String settlementNo) {
		this.settlementNo = settlementNo;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Date getVocDate() {
		return vocDate;
	}
	public void setVocDate(Date vocDate) {
		this.vocDate = vocDate;
	}
	public Timestamp getOrderDateTime() {
		return orderDateTime;
	}
	public void setOrderDateTime(Timestamp orderDateTime) {
		
		this.orderDateTime = orderDateTime;
	}
	public Date getPropClient() {
		return propClient;
	}
	public void setPropClient(Date propClient) {
		this.propClient = propClient;
	}
	public Timestamp getTradeDateTime() {
		return tradeDateTime;
	}
	public void setTradeDateTime(Timestamp tradeDateTime) {
		this.tradeDateTime = tradeDateTime;
	}
	public Long getBuyQty() {
		
		if (getTradedQuantity() != null && (getTransactionType().equals("B") || getTransactionType().equals("1"))){
			return Long.parseLong(getTradedQuantity());
		}else{
			return 0L;
		}
	}
	public void setBuyQty(Long buyQty) {		
		this.buyQty = buyQty;		
	}
	public Long getSellQty() {
		if (getTradedQuantity() != null && (getTransactionType().equals("S")|| getTransactionType().equals("2"))){
			return Long.parseLong(getTradedQuantity());
		}else{
			return 0L;
		}
	}
	public void setSellQty(Long sellQty) {
		this.sellQty = sellQty;
	}
	public String getApplicableSymbol() {
		return applicableSymbol;
	}
	public void setApplicableSymbol(String applicableSymbol) {
		this.applicableSymbol = applicableSymbol;
	}
	public String getApplicableSeries() {
		return applicableSeries;
	}
	public void setApplicableSeries(String applicableSeries) {
		this.applicableSeries = applicableSeries;
	}
	public String getApplicableSettlementNo() {
		return applicableSettlementNo;
	}
	public void setApplicableSettlementNo(String applicableSettlementNo) {
		this.applicableSettlementNo = applicableSettlementNo;
	}
	public String getApplicableSettlementType() {
		return applicableSettlementType;
	}
	public void setApplicableSettlementType(String applicableSettlementType) {
		this.applicableSettlementType = applicableSettlementType;
	}
	public Long getApplicableBusinessLineNo() {
		return applicableBusinessLineNo;
	}
	public void setApplicableBusinessLineNo(Long applicableBusinessLineNo) {
		this.applicableBusinessLineNo = applicableBusinessLineNo;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getClientState() {
		return clientState;
	}
	public void setClientState(String clientState) {
		this.clientState = clientState;
	}	
}
