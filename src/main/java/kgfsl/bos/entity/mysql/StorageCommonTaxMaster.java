package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CommonTaxMaster",
indexes= {@Index(name = "commonTaxIndx", columnList = "businessLineNo,transactionType,transactionSubType" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "commTaxBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageCommonTaxMaster extends BaseEntity {
	   
	   @Id
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   @Column(unique = true)
	   private Long id;
	   private long businessLineNo;
	   private String transactionType;
	   private String transactionSubType;
	   private byte[] taxDetail; 
	   private Date createdAt;
	   private String createdBy;
	   private Date approvedAt;
	   private String approvedBy;
	   private Date modifiedAt;
	   private String modifiedBy;
	   private String defaultFlag;
}
