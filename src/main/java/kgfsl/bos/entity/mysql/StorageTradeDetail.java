package kgfsl.bos.entity.mysql;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import kgfsl.bos.constant.CommonConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TradeDetail",
indexes= {@Index(name = "tradeDtlIndx", columnList = "businessLineNo,vocDate,settlementNo,settlementType,clientCode,symbol,seriesCode,tradeNo,orderNo" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "tradeDetailBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageTradeDetail extends BaseEntity {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    Long id;
	Long businessLineNo;
	Date vocDate;
	String clientCode;
	String uccCode;		
	Long scripCode;
	String symbol;
	String seriesCode;		
	Long applicableBusinessLineNo;
	String applicableSettlementNo;
	String applicableSettlementType;
	String applicableSymbol;
	String applicableSeries;	
	String tradeNo;
	String orderNo;
	String tradedTime;	
	Timestamp tradeActyDateTime;
	Timestamp orderDateTime;
	String transactionType;
	Long tradedQuantity;
	Double tradedPrice;
	String proprietryClient;	
	String settlementNo;  
	String settlementType;
	Date settlementDate;
	String ctclId;	
	@Builder.Default
	Long buyQuantity = 0L;
	@Builder.Default
	Long sellQuantity = 0L;
	@Builder.Default
	Long intraDayBuyQuantity = 0L;
	@Builder.Default
	Long intraDaySellQuantity  = 0L;
	@Builder.Default
	Long deliveryBuyQuantity = 0L;
	@Builder.Default
	Long deliverySellQuantity = 0L;	
	Double deliveryTradeValue;	
	Double intraDayTradeValue;	
	Long contractNo;
	@Builder.Default
	String contractGenerateStatus = "N";
	@Builder.Default
	String amendTradeIndicator = "N";		
	String batchId;
	Date createdAt;
	String createdBy;
	String processType;
	String clientState;
	
	@Transient
	@Builder.Default
	Long cummTradedQty =0L;	
	@Transient
	Double deliveryBuyTradeValue;
	@Transient
	Double deliverySellTradeValue;
	@Transient
	Double intraDayBuyTradeValue;
	@Transient
	Double intraDaySellTradeValue;
	@Transient
	String debentureFlag;

	public StorageTradeDetail(Long applicableBusinessLineNo,Date vocDate,String uccCode,String clientCode,Long scripCode,
			String applicableSettlementNo,String applicableSettlementType,Long tradedQuantity,
			Long buyQty,Long sellQty,Long deliveryBuyQuantity,
			Long intraDayBuyQuantity,Long deliverySellQuantity, Long intraDaySellQuantity
			) throws ParseException
	{			
		this.applicableBusinessLineNo= applicableBusinessLineNo;
		this.vocDate = CommonConstants.YYYY_MM_DD.parse(vocDate.toString());
		this.uccCode = uccCode;
		this.clientCode =clientCode;
		this.scripCode=scripCode;
		this.tradedQuantity =tradedQuantity;
		this.applicableSettlementNo = applicableSettlementNo;
		this.applicableSettlementType=applicableSettlementType;
		this.buyQuantity = buyQty;
		this.sellQuantity = sellQty;		
		this.deliveryBuyQuantity= deliveryBuyQuantity;
		this.intraDayBuyQuantity= intraDayBuyQuantity;
		this.deliverySellQuantity = deliverySellQuantity;
		this.intraDaySellQuantity = intraDaySellQuantity;
	}	
}
