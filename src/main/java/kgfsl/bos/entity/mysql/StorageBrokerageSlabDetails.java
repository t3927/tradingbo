package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "BSG_BRKG_ASGN_SLAB_GRP_T")
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "brokSlabBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageBrokerageSlabDetails extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private String schemeId;
	private Double fromValue;
	private Double toValue;
	private String buyBrokerage;
	private String sellBrokerage;
	private String minBrokerage;
	private String maxBrokerage;
	private String computeBy;
	private String computeType;
	private String legType;
	private String computeOn;
	private String multiplier;
	private Date effFromDate;
	private Date effToDate;
}
