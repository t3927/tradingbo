package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SettlementMaster",
indexes= {@Index(name = "settlementMastIndx", columnList = "businessLineNo,settlementNo,settlementType" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "seriesMastBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageSettlementMaster extends BaseEntity
{	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
	
	private Date deliveryInDate;
	
	private Date deliveryOutDate;
	
	private Date tradeFromDate;
	
	private Date tradeToDate;
	
	private Date obligationDate;
	
	private Date payInDate;
	
	private Date payOutDate;	
	
	private String auctionMergerWithSettlementNo;
	
	private String dispYear;	
	
	private Date fileDate;	
	
	private Long businessLineNo;	
	
	private String exchangeCode;	
	
	private String settlementNo;	
	
	private String segmentCode;
	
	private String settlementType;
	
}

