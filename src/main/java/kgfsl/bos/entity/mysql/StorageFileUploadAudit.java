package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "FileUploadAudit")
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "fileAuditBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageFileUploadAudit extends BaseEntity{
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;	
	private String fileCode;
	private Date fileDate;
	private String fileName;
	private String settlementNo;
	private Long inValidRecords;
	private Long validRecords;
	private Long totalRecords;
	private byte[] logFile;
	private Date processDate;
	private Date uploadAt;
	private String uploadBy;
	private String uploadStatus;
	private Date startTime;
	private Date endTime;	
}
