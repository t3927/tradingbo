package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity {
		
	private String createdBy;
	
	private Date createdAt;
	
	private String modifiedBy;
	
	private Date modifiedAt;
	
	private String approvedBy;
	
	private Date approvedAt;
}
