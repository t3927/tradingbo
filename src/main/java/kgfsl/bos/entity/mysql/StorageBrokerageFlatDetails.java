package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "BrokerageFlatDetails")
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "brokFlatBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageBrokerageFlatDetails extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private Long schemeId;
	private Double upperLimit;
	private String buyBrokerage;
	private String sellBrokerage;
	private String legType;
	private String oneSideIndicator;
	private String computeBy;
	private Date effFromDate;
	private Date effToDate;	
	private String segmentGrp;
	
}
