package kgfsl.bos.entity.mysql;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "FILECONFIG",
indexes= {@Index(name = "fileConfigIndx", columnList = "fileCode,businessLineNo" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "fileConfigBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageFileConfig extends BaseEntity {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
	
	private String fileCode;

	private String fileDescription;
	
	private String fileType;

	private String delimiter;

	private String fileName;	

	private String fileNamePattern;

	private Long columnCount;

	private Long headerCount;

	private String remarks;

	private String apiName;
	
	private String rejectionType;
	
	private Long businessLineNo;

	@Transient
	@JsonIgnore
	public String getFileName(Date fileDate) {
		if ((getFileNamePattern() != null && getFileNamePattern() != "") && (getFileName() != null && getFileName() != "")) {
			try {
				String dynamicName = getFileNamePattern().trim();
				SimpleDateFormat sdf = new SimpleDateFormat(dynamicName);
				return getFileName().replaceAll(dynamicName, sdf.format(fileDate));
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return getFileName();
	}
}

