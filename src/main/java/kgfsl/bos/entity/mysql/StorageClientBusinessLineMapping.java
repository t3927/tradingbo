package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ClientBusinessLineMapping", indexes= {@Index(name = "clntBusMapIndx", columnList = "clientCode,businessLineNo,inactiveIndr" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "clientBusLnMapBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageClientBusinessLineMapping extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private String clientCode;
	private Long businessLineNo;
	private String exchApprovedStatus;
	private String inactiveIndr;
	private Date inactiveDate;
	private Date lastTradedDate;
}
