package kgfsl.bos.entity.mysql;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ScripMaster",
indexes= {@Index(name = "scripMastIndx", columnList = "stkName,isinCode" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "scripMastBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageScripMaster  extends BaseEntity{
		
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
	
	private Long scripCode;

	private String stkName;
	
	private String isinCode;
	
	private String issueSize;
	
	private String ricCode;
	
	private String bloombergCode;
	
	private String cusipCode;
	
	private String sedolCode;
	
	private String adrGdr;
	
	private String gdrRatio;
	
	private Boolean liquid;
	
	private Boolean sttApplicable;
	
	private Integer stkMultiplier;
	
	private String instrumentType;
	
	private Boolean etfFlag;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "scripCode", referencedColumnName = "scripCode")	
	private List<StorageScripDetail> scripDetail;
}
