package kgfsl.bos.entity.mysql;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "BrokerageSchemeMaster")
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "brokSchemeBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageBrokerageSchemeMaster extends BaseEntity {
	
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		@Column(unique = true)
	    private Long id;
		private long businessLineNo;
		private String segmentGroup;
		private String schemeCode;
		private String schemeDesc;
		private String categoryType ;
		private String categorySubType ;
		
		private String roundOff ;
		private String brokAdvance ;
		private String computeBasis ;
		private String computeOn ;
		private String turnoverOn ;
		private String slabBasis ;
		private Long scripCode ;
		
		private Date effFromDate;
		private Date effToDate;
		
		@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
		@JoinColumn(name = "bsrId", referencedColumnName = "Id")
		private List<StorageBrokerageSlabDetails> brkgAssgnSlabGrp;
		
		@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
		@JoinColumn(name = "bsrId", referencedColumnName = "Id")
		private List<StorageBrokerageFlatDetails> brkgAssgnFlatGrp;
}
