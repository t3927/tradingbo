package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TradeChargesDetail",
indexes= {@Index(name = "tradeChargeIndx", columnList = "businessLineNo,vocDate,settlementNo,settlementType,clientCode,symbol,seriesCode,tradeNo,orderNo" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "tradeChargeBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageTradeChargesDetail extends BaseEntity {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
	Long businessLineNo;
	Date vocDate;
	String clientCode;
	String settlementNo;  
	String settlementType;
	Long scripCode;
	String symbol;
	String seriesCode;	
	String tradeNo;
	String orderNo;
	
	String gstStateCode;
	String stampStateCode;
	
	Double cgstPercent;
	Double sgstPercent;
	Double igstPercent;
	Double ugstPercent;
	
	Double cgst;
	Double sgst;
	Double igst;
	Double ugst;
	Double gstTotal;
	
	Double stampDuty;
	Double intraDayStampDuty;
	Double turnoverCharge;
	Double sebiCharge;
	Double stt;
	Double intraDayStt;
	Double transactionCharge;
	
	Long tradingSchemeId;
	Long deliverySchemeId;
	
	Double intraDayBuyBrokerageRate;
	Double intraDaySellBrokerageRate;
	Double deliveryBuyBrokerageRate;
	Double deliverySellBrokerageRate;
	Double buyBrokerage;
	Double sellBrokerage;
	Double deliveryBuyBrokerage;
	Double deliverySellBrokerage;
	Double intraDayBuyBrokerage;
	Double intraDaySellBrokerage;
	Double brokerageVal;
	
	String batchId;	
	
	@Transient
	Double dlyBuyValue;
	@Transient
	Double dlySellValue;
	@Transient
	Double trdBuyValue;
	@Transient
	Double trdSellValue;
	@Transient
	Double totStampValue;
	

	public StorageTradeChargesDetail(String clientCode,String symbol, String seriesCode, Double totStamp) 
	{
		this.clientCode=clientCode;
		this.symbol=symbol;
		this.seriesCode=seriesCode;
		this.stampDuty = totStamp;		
	}
	
}
