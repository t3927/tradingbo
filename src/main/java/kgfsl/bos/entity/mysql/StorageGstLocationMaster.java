package kgfsl.bos.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "GstLocationMaster",
indexes= {@Index(name = "gstLocIndx", columnList = "locationCode,locationName,gstNo" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "gstLocationBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageGstLocationMaster extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private String locationCode;
	private String locationName;
	private String gstNo;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String phoneNo1;
	private String phoneNo2;
	private String fax;
	private String email;
	private String unionTerritory;
	private String gstDefaultFlag;	
}
