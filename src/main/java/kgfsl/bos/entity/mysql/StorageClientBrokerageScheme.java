package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ClientBrokerageScheme",
indexes= {@Index(name = "clntBrokSchmIndx", columnList = "businessLineNo,clientCode,effFromDate" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "clientBrokSchemeBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageClientBrokerageScheme extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private long businessLineNo;
	private String clientCode;
	private String segmentGroup;
	private String eqTradingSchemeId;
	private String eqDeliverySchemeId;
	private String drDeliverySchemeId;
	private String drFutureIndexSingleSideSchemeId;
	private String drFutureIndexExpirySchemeId;
	private String drFutureStockSingleSchemeId;
	private String drFutureStockExpirySchemeId;
	private String drOptionIndexSchemeId;
	private String drOptionStockSchemeId;
	private String drExAsgnIndexSchemeId;
	private String drExAsgnStockSchemeId;
	private String cdFutureSingleSideSchemeId;
	private String cdFutureOnExpirySchemeId;
	private String cdFutureIrcSingleSideSchemeId;
	private String cdFutureIrcOnExpirySchemeId;
	private String cdOptionSchemeId;
	private String cdOptExpirySchemeId;
	private String oneSideIndicator;	
	
	private Date effFromDate;
	private Date effToDate;

}
