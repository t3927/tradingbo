package kgfsl.bos.entity.mysql;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "ScripDetail",
indexes= {@Index(name = "scripDtlIndx", columnList = "businessLineNo,symbol,seriesCode,effFromDate" )})
@JsonInclude(Include.NON_NULL)	
@Data
@Builder(builderMethodName = "scripDtlBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageScripDetail extends BaseEntity { 
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	
	private String exchangeCode;
	
	private String symbol;	
	
	private String seriesCode;
	
	private Date effFromDate;
	
	private Date effToDate;	
	
	private String dispSymbol;	
	
	private Date inActiveDate;	
	
	private String businessLineNo;
	
	private String applicableSymbol;	
	
	private String applicableSeries;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scripCode") 
	private StorageScripMaster scrip;	
}

