package kgfsl.bos.entity.mysql;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ClientMaster")
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "clientMasterBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageClientMaster extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique = true)
    private Long id;
	private String clientCode;
	private String pan;
	private String uccCode;
	private String clientName;
	private String clientType;
	private Date inActiveDate;
	private String gstFlag;
	private String sttFlag;
	private String stampDutyFlag;
	private String transChargesFlag;
	private String sebiFeesFlag;
	private String mtfApprovedIndr;
	private String corrAddress1;
	private String corrAddress2;
	private String corrAddress3;
	private String corrCity;
	private String corrStateName;
	private String corrCountry;
	private String corrPin;
	private String clientStateCode;	
}
