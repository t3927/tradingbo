package kgfsl.bos.entity.mysql;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SettlementType",
indexes= {@Index(name = "setlTypeIndx", columnList = "businessLineNo,settlementType" )})
@JsonInclude(Include.NON_NULL)
@Data
@Builder(builderMethodName = "setlTypeBuilder")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StorageSettlementType extends BaseEntity{
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(unique = true)
    private Long id;
	private Long businessLineNo;
	private String settlementType;
	private String settlementName;
	private String applicableSetlType;
	private String processType;
	private String exchangeCode;
		
}
