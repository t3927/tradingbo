package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageClientMaster;

public interface ClientMasterRepository extends JpaRepository<StorageClientMaster, Long>{

	StorageClientMaster findByUccCode(String uccCode);
	
	@Query(nativeQuery =true,value = "SELECT * FROM ClientMaster scr WHERE scr.uccCode = :uccCode")  
    List<StorageClientMaster> findByUccCode(@Param("uccCode") List<String> uccCode);

}
