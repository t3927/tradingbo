package kgfsl.bos.repository.mysql;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageFileUploadAudit;

public interface FileUploadAuditRepository extends JpaRepository<StorageFileUploadAudit, Long> {

	StorageFileUploadAudit findTop1ByFileCodeAndFileDateOrderByUploadAtDesc(String fileCode, Date filedate);
}
