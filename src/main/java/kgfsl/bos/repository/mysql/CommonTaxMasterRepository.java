package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageCommonTaxMaster;

public interface CommonTaxMasterRepository extends JpaRepository<StorageCommonTaxMaster, Long> {

	List<StorageCommonTaxMaster> findByBusinessLineNoAndTransactionType(Long busLineNo,String tranType);
}
