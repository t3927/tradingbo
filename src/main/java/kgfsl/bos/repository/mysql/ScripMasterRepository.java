package kgfsl.bos.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageScripMaster;

public interface ScripMasterRepository extends JpaRepository<StorageScripMaster, Long> {

}
