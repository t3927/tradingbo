package kgfsl.bos.repository.mysql;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageTradeDetail;

public interface TradeDetailRepository extends JpaRepository<StorageTradeDetail, Long> {
	
	List<StorageTradeDetail> findByApplicableBusinessLineNoAndVocDateAndApplicableSettlementNo(Long busLineNo,Date vocDate,String setlNo);
	
	@Query(nativeQuery =true,value = "Select S1.transactionType,S1.applicableBusinessLineNo,S1.vocDate,S1.uccCode,"+
            "S1.scripCode,S1.applicableSettlementNo ,S1.applicableSettlementType,S1.clientCode,S1.tradeNo t_tradeNo,S1.orderNo t_orderNo,S1.tradedQuantity, "+
		    "(Select Sum(tradedQuantity) trdQty From TradeDetail S2 Where S2.id<= S1.id and S2.transactionType = S1.transactionType "+
            "and S2.applicableBusinessLineNo = S1.applicableBusinessLineNo and S2.vocDate = S1.vocDate and S2.uccCode = S1.uccCode and S2.scripCode = S1.scripCode "+
            "and S2.applicableSettlementNo = S1.applicableSettlementNo and S2.applicableSettlementType = S1.applicableSettlementType and S2.clientCode = S1.clientCode "+
            "group by  transactionType,applicableBusinessLineNo,vocDate,uccCode,scripCode,applicableSettlementNo,applicableSettlementType,clientCode"+
			") cummTradedQty From TradeDetail S1 where S1.applicableBusinessLineNo = :busLineNo and S1.vocDate = :vocDate and S1.applicableSettlementNo = :setlNo"
		 )
	List<Object> cummTradeDetails(@Param("busLineNo") Long busLineNo,@Param("vocDate") Date vocDate,@Param("setlNo") String setlNo);

	
}
