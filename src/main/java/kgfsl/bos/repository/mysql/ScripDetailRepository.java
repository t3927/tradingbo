package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kgfsl.bos.entity.mysql.StorageScripDetail;

@Repository
public interface ScripDetailRepository extends JpaRepository<StorageScripDetail, Long>{
	
	List<StorageScripDetail> findByBusinessLineNo(Long busLineNo);
		
    @Query(nativeQuery =true,value = "SELECT * FROM StorageScripDetail scr WHERE scr.businessLineNo = :busLineNo AND scr.symbol IN (:symbols) AND effToDate is null")  
    List<StorageScripDetail> findByBusinessLineNoAndSymbolInAndEffToDateIsNull(@Param("busLineNo") Long busLineNo,@Param("symbols") List<String> symbols);

}
