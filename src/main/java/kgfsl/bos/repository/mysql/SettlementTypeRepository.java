package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageSettlementType;

public interface SettlementTypeRepository extends JpaRepository<StorageSettlementType, Long> {
	
	@Query(nativeQuery =true,value = "SELECT * FROM SettlementType WHERE businessLineNo = :busLineNo AND ProcessType != :prcsType AND applicableSetlType != ''")
	List<StorageSettlementType> findbyBusinessLineNoAndProcessTypeNot(@Param("busLineNo") Long busLineNo,@Param("prcsType") String prcsType);

}
