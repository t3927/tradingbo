package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageClientBusinessLineMapping;

public interface ClientBusinessLineMappingRepository extends JpaRepository<StorageClientBusinessLineMapping, Long> {

	@Query(nativeQuery =true,value = "SELECT * FROM ClientBusinessLineMapping WHERE businessLineNo = :busLineNo AND clientCode = :clientCode")  
    List<StorageClientBusinessLineMapping> findByBusinessLineNoAndClientCode(@Param("busLineNo") Long busLineNo,@Param("clientCode") List<String> clientCode);
}
