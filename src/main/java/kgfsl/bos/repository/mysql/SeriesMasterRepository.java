package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageSeriesMaster;

public interface SeriesMasterRepository extends JpaRepository<StorageSeriesMaster, Long> {

	@Query(nativeQuery =true,value = "SELECT * FROM SeriesMaster WHERE businessLineNo = :busLineNo AND seriesCode = :seriesCode")  
    List<StorageSeriesMaster> findByBusinessLineNoAndSeriesCode(@Param("busLineNo") Long busLineNo,@Param("seriesCode") List<String> seriesCode);
	
	@Query(nativeQuery =true,value = "SELECT * FROM SeriesMaster WHERE businessLineNo = :busLineNo AND debentureFlag = :debentureFlag")  
    List<StorageSeriesMaster> findByBusinessLineNoAndDebentureFlag(@Param("busLineNo") Long busLineNo,@Param("debentureFlag") String debentureFlag);	
	
	
}
