package kgfsl.bos.repository.mysql;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageSettlementMaster;

public interface SettlementMasterRepository extends JpaRepository<StorageSettlementMaster,Long> {

	List<StorageSettlementMaster> findByTradeFromDateAndBusinessLineNo(Date startDate,Long busLineNo);
	
	@Query(value = "SELECT * FROM SettlementMaster sm, SettlementType st "
			+ " where sm.SettlementType = st.applicableSetlType and sm.exchangeCode = :interOpExchCode and sm.tradeFromDate = :fromDate and  st.exchangeCode = :convExchCode "
			+ "and st.processType != :processType ",
	        nativeQuery = true)
	List<StorageSettlementMaster> getInterOpSetlDet(@Param("interOpExchCode") String interOpExchCode,@Param("convExchCode") String convExchCode,
			@Param("fromDate") Date fromDate,@Param("processType") String processType);
	
	

}
