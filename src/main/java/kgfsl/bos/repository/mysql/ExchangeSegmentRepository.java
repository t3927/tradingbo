package kgfsl.bos.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageExchangeSegmentMaster;

public interface ExchangeSegmentRepository extends JpaRepository<StorageExchangeSegmentMaster, Long> {
	
	StorageExchangeSegmentMaster findByInterOperabilityFlag(String intOperableFlag);
	
	StorageExchangeSegmentMaster findByExchangeCode(String exchCode);
	
	StorageExchangeSegmentMaster findByBusinessLineNo(Long busLineNo);

}
