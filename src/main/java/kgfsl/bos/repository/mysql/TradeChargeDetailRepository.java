package kgfsl.bos.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageTradeChargesDetail;

public interface TradeChargeDetailRepository extends JpaRepository<StorageTradeChargesDetail, Long> {
	
}
