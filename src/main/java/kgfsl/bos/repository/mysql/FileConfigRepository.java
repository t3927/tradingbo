package kgfsl.bos.repository.mysql;

import org.springframework.data.jpa.repository.JpaRepository;

import kgfsl.bos.entity.mysql.StorageFileConfig;

public interface FileConfigRepository extends JpaRepository<StorageFileConfig, Long> {
	
	StorageFileConfig findByFileCode(String fileCode);

}
