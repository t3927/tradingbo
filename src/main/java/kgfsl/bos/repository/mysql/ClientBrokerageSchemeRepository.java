package kgfsl.bos.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kgfsl.bos.entity.mysql.StorageClientBrokerageScheme;

public interface ClientBrokerageSchemeRepository extends JpaRepository<StorageClientBrokerageScheme, Long>{
	
	@Query(nativeQuery =true,value = "SELECT * FROM ClientBrokerageScheme WHERE businessLineNo = :busLineNo AND clientCode = :clientCode")  
    List<StorageClientBrokerageScheme> findByBusinessLineNoAndClientCode(@Param("busLineNo") Long busLineNo,@Param("clientCode") List<String> clientCode);	
}
